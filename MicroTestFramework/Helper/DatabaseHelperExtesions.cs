﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MicroAutoTestFramework.Helper
{
    public static class DatabaseHelperExtesions
    {

        public static SqlConnection DBConnect(this SqlConnection sqlConnection, string connectionString)
        {
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                return sqlConnection;
            }
            catch (Exception e)
            {
                LogHelper.Write("ERROR :: " + e.Message);
            }

            return null;
        }

        public static void DBClose(this SqlConnection sqlConnection)
        {
            try
            {
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                LogHelper.Write("ERROR :: " + e.Message);
            }
        }

        public static DataTable ExecuteQuery( this SqlConnection sqlConnection, string queryString)
        {
            DataSet dataset;

            try
            {
                if (sqlConnection == null || (sqlConnection != null && (sqlConnection.State == ConnectionState.Broken || sqlConnection.State == ConnectionState.Closed)))
                    sqlConnection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = new SqlCommand(queryString, sqlConnection);
                dataAdapter.SelectCommand.CommandType = CommandType.Text;

                dataset = new DataSet();
                dataAdapter.Fill(dataset, "table");
                sqlConnection.Close();

                return dataset.Tables["table"];
            }
            catch (Exception e)
            {
                dataset = null;
                sqlConnection.Close();
                LogHelper.Write("ERROR :: " + e.Message);

                return null;
            }
            finally
            {
                sqlConnection.Close();
                dataset = null;
            }
        }
    }
}
