﻿using System;
using System.IO;


namespace MicroAutoTestFramework.Helper
{
    public class LogHelper
    {
        private static string _logFileName = string.Format("{0:yyyymmddhhmmss}", DateTime.Now);
        private static StreamWriter _streamW = null;

        public static void CreateLogFile()
        {
            string dir = @"E:\Testy\MicroTestFramework\";

            if (Directory.Exists(dir))
            {
                _streamW = File.AppendText(dir + _logFileName + ".log");
            }
            else
            {
                Directory.CreateDirectory(dir);
                _streamW = File.AppendText(dir + _logFileName + ".log");
            }
        }

        public static void Write(string logMessage)
        {
            _streamW.Write("\n{0} {1}", DateTime.Now.ToLongTimeString() ,DateTime.Now.ToLongDateString());
            _streamW.Write("  {0}", logMessage);
            _streamW.Flush();
        }
    }
}
