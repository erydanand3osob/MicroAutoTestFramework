﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace MicroAutoTestFramework.Helper
{
    public class ExcelHelper
    {

        private static List<ExcelDataCollection> _dataECollection = new List<ExcelDataCollection>();

        /// <summary>
        /// Storing all the excel Values to in-memory collections
        /// </summary>
        /// <param name="fileName"></param>
        public static DataTable ExcelToTable(string filePathEndName)
        {
            //stworzyc metode 
            int index = 1;

            using (XLWorkbook wBook = new XLWorkbook(filePathEndName))
            {
                DataTable dTable = new DataTable();

                //Read the first Sheet from index value from Excel file.
                IXLWorksheet workSheet = wBook.Worksheet(index);

                //Loop through the Worksheet rows.
                bool firstRow = true;
                foreach (IXLRow row in workSheet.Rows())
                {
                    //Use the first row to add columns to DataTable.
                    if (firstRow)
                    {
                        foreach (IXLCell cell in row.Cells())
                        {
                            dTable.Columns.Add(cell.Value.ToString());
                        }
                        firstRow = false;
                    }
                    else
                    {
                        //Add rows to DataTable.
                        dTable.Rows.Add();
                        int i = 0;
                        foreach (IXLCell cell in row.Cells())
                        {
                            dTable.Rows[dTable.Rows.Count - 1][i] = cell.Value.ToString();
                            i++;
                        }
                    }
                }
                return dTable;
            }
        }
        
        public static void ExtractDataFromExcel(string filePathEndName)
        {
            DataTable wTable = ExcelToTable(filePathEndName);
            for (int row = 1; row <= wTable.Rows.Count; row++)
            {
                for (int col = 0; col < wTable.Columns.Count; col++)
                {
                    ExcelDataCollection dtTable = new ExcelDataCollection()
                    {
                        rowNumber = row,
                        colName = wTable.Columns[col].ColumnName,
                        colValue = wTable.Rows[row - 1][col].ToString()
                    };
                    _dataECollection.Add(dtTable);
                }
            }

        }

        /// <summary>
        /// read data from collection
        /// </summary>
        /// <param name="rowNumber"> Row number in collection data </param>
        /// <param name="columnName"> Column name in collection data </param>
        /// <returns></returns>
        public static string ReadData(int rowNumber, string columnName)
        {
            try
            {
                string data = (from colData in _dataECollection
                               where colData.colName == columnName && colData.rowNumber == rowNumber
                               select colData.colValue).SingleOrDefault();
                                    
                return data.ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }      
    }

    /// <summary>
    /// custom class for colections data to read
    /// </summary>
    public class ExcelDataCollection
    {
        public int rowNumber { get; set; }
        public string colName { get; set; }
        public string colValue { get; set; }
    }
}
