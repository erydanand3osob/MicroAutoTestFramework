﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace MicroAutoTestFramework.Configuration
{
    public class ConfigReader
    {
        public static void SetFrameworkSettings()
        {
            XPathItem aut;
            XPathItem testType;
            XPathItem isLog;
            XPathItem isReport;
            XPathItem buildName;
            XPathItem logPath;
            XPathItem appConnection;

            string strFileName = Environment.CurrentDirectory.ToString() + "\\Configuration\\GlobalConfiguration.xml";
            FileStream stream = new FileStream(strFileName, FileMode.Open);
            XPathDocument document = new XPathDocument(stream);
            XPathNavigator navigator = document.CreateNavigator();

            aut = navigator.SelectSingleNode("MicroAutoFramework/RunSettings/AUT");
            buildName = navigator.SelectSingleNode("MicroAutoFramework/RunSettings/BuildName");
            testType = navigator.SelectSingleNode("MicroAutoFramework/RunSettings/TestType");
            isLog = navigator.SelectSingleNode("MicroAutoFramework/RunSettings/IsLog");
            isReport = navigator.SelectSingleNode("MicroAutoFramework/RunSettings/IsReport");
            logPath = navigator.SelectSingleNode("MicroAutoFramework/RunSettings/LogPath");
            appConnection = navigator.SelectSingleNode("MicroAutoFramework/RunSettings/ApplicationDb");

            Settings.AUT = aut.Value.ToString();
            Settings.BuildName = buildName.Value.ToString();
            Settings.TestType = testType.Value.ToString();
            Settings.IsLog = isLog.Value.ToString();
            Settings.IsReport = isReport.Value.ToString();
            Settings.LogPath = logPath.Value.ToString();
            Settings.AppConnectionString = appConnection.Value.ToString();
        }
    }
}
