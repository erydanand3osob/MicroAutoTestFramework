﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;

namespace MicroAutoTestFramework.Base
{
    public class Base
    {
        public BasePage CurrentPage
        {
            get
            {
                return (BasePage)ScenarioContext.Current["currentPage"];
            }

            set
            {
                ScenarioContext.Current["currentPage"] = value;
            }
        }

        private IWebDriver _driver { get; set; }

        /// <summary>
        /// Create instance of the page and initialize page
        /// </summary>
        /// <typeparam name="TPage">The Element type of the Page</typeparam>
        /// <returns>Actual instance of page</returns>
        protected TPage GetInstance<TPage>() where TPage : BasePage, new()
        {
            TPage pageInstance = new TPage()
            {
                _driver = DriverContext.Driver
            };

            PageFactory.InitElements(DriverContext.Driver, pageInstance);

            return pageInstance;
        }

        /// <summary>
        /// Check if actual page is current page
        /// </summary>
        /// <typeparam name="TPage">The element type of the Page</typeparam>
        /// <returns>Actual page</returns>
        public TPage As<TPage>() where TPage : BasePage
        {
            return (TPage)this;
        }
    }
}
