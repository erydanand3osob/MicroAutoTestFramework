﻿Feature: Login
	Check if the Login functionality is working
	 as expected with different permutiations
	  and combinations of data

@smoke @positive
Scenario: Check Login with correct username and password
	Given I have navigated to the application
	And I see application opened
	Then I click login link
	When I enter UserName and Password 
	| UserName | Password |
	| admin    | password |
	Then I click login button
	Then I should see the username with hello

Scenario: Check When pass invalid user name
	Given I have navigated to the application
	And I see application opened
	Then I click login link
	When I enter UserName and Password 
	| UserName | Password |
	| admin    | password |
	Then I click login button
	Then I should see validation message 

Scenario: Check When pass correct username and invalid password
	Given I have navigated to the application
	And I see application opened
	Then I click login link
	When I enter UserName and Password 
	| UserName | Password |
	| admin    | password |
	Then I click login button
	Then I should see validation message 

Scenario: Check Keeping Password blank
	Given I have navigated to the application
	And I see application opened
	Then I click login link
	When I enter UserName and Password 
	| UserName | Password |
	| admin    | password |
	Then I click login button
	Then I should see validation message 
