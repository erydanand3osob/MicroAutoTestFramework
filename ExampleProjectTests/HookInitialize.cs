﻿using MicroAutoTestFramework.Base;
using MicroAutoTestFramework.Configuration;
using MicroAutoTestFramework.Helper;
using TechTalk.SpecFlow;

namespace ExampleProjectTests
{
    [Binding]
    public class HookInitialize: TestInitializeHook
    {
        public HookInitialize() : base(BrowserType.Firefox)
        {
            InitializeSettings();
            Settings.ApplicationConn = Settings.ApplicationConn.DBConnect(Settings.AppConnectionString);

        }

        [BeforeFeature]
        public static void TestStart()
        {
            HookInitialize init = new HookInitialize();
        }
    }
}
