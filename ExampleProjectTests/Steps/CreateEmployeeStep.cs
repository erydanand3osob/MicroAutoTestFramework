﻿using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using MicroAutoTestFramework.Base;
using ExampleProjectTests.Pages;

namespace ExampleProjectTests.Steps
{
    [Binding]
    internal class CreateEmployeeStep : BaseStep
    {
        [Then(@"I enter following details")]
        public void ThenIEnterFollowingDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            CurrentPage.As<EmployeeDataEntryFormPage>().CreateEmployee(data.Name, data.Salary.ToString(), data.DurationWorked.ToString(), data.Grade.ToString(), data.Email);               
        }
    }
}
