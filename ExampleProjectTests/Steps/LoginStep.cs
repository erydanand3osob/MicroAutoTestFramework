﻿using ExampleProjectTests.Pages;
using MicroAutoTestFramework.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace ExampleProjectTests.Steps
{
    [Binding]
    public class LoginStep : BaseStep
    {



        [When(@"I enter UserName and Password")]
        public void WhenIEnterUserNameAndPassword(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            CurrentPage.As<LoginPage>().LoginData(data.UserName, data.Password);
        }


        [Then(@"I should see the username with hello")]
        public void ThenIShouldSeeTheUsernamewWithHello()
        {
            if (CurrentPage.As<HomePage>().GetLoggedInUser().Contains("admin"))
                System.Console.WriteLine("Success login");
            else
                System.Console.WriteLine("unsuccessfull login");
        }

    }
}
